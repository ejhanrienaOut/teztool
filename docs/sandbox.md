# Teztool: Sandboxes

Generally, the commands used to manage a sandbox node are mostly identical to managing 
a [normal node](./nodes.md), simply create your node with the `sandbox` network.


## Sandbox Baker

You can start a Sandbox with a Baker already running by providing the `--baker-identity`:
The baker address must be one of the standard bootstrap accounts ( bootstrap1...5 ).

In addition to that, you can specify protocol parameters, like time between blocks in seconds with the flag 
`--time-between-blocks`

```
$ teztzool create <name> --baker-identity bootstrap1 --time-between-blocks 10
```

For all possible protocol parameters that you can change, view:

```
$ teztool create --help
```

If you already have a sandbox running, and want to start or stop the baker, do:

```
$ teztool <name> baker baker start|stop
```

## Tezos Client

When you start a sandbox, Teztool will initialise a client with the activator and bootstrap accounts,
If you want to use the tezos-client that teztool provides, run:

```
$ teztool <name> client <command...>

# for example:
$ teztool <name> client list known addresses
bootstrap5: tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv (unencrypted sk known)
bootstrap4: tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv (unencrypted sk known)
bootstrap3: tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU (unencrypted sk known)
bootstrap2: tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN (unencrypted sk known)
bootstrap1: tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx (unencrypted sk known)
activator: tz1TGu6TN5GSez2ndXXeDX6LgUDvLzPLqgYV (unencrypted sk known)
```

In case you want to use a different client, ( e.g. installed via snap ) you can import these accounts with:

```
tezos-client import secret key activator unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6
tezos-client import secret key bootstrap1 unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh
tezos-client import secret key bootstrap2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo
tezos-client import secret key bootstrap3 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ
tezos-client import secret key bootstrap4 unencrypted:edsk2uqQB9AY4FvioK2YMdfmyMrer5R8mGFyuaLLFfSRo8EoyNdht3
tezos-client import secret key bootstrap5 unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm
```
# Baking

Teztool can start baking and endorsing daemons for any network with

```
$ teztool <name> baker start 
```

Or for Sandboxes with

```
$ teztool <name> sandbox baker start <address>
```

For normal nodes, the baker will bake for any address currently registered in the client,
you can manage those addresses with

```
$ teztool <name> client <command>
```

To bake with a remote signer, simply add an address like this:

```
$ teztool <name> client import secret key <keyAlias> tcp://<SignerIp>:<port>/tz1....
```

and start/restart your baker containers:

```
$ teztool <name> baker  start|stop

# afterwards, check logs with:
$ teztool <name> baker logs
```

If you want to start using the `tezos-signer` there is a small tutorial to get you familiar with it [here](./signer.md)

# Teztool: Signer Introduction

This is a small Tutorial that you can execute on your local machine, that will get you familiar with using the
`tezos-signer`

A typical remote-signer setup looks like this:

![Remote Signer Setup](./assets/SignerSetup.png)

with the Node and baker running on the same machine, often a VPS or a hosted Server, and the Signer running on a smaller machine, in a secure Location.

The private keys for the Baker are stored only on the signer-machine, so that they are still secure even if your node gets compromised.

In practice these Machine might be connected via a SSH tunnel or a VPN like:

* https://www.wireguard.com/
* https://www.tinc-vpn.org/

This Setup however is beyond the Scope of this Tutorial, and we will simulate this locally with a dummy interface.


## Setup a Dummy IP/Interface

( only for local testing, read above! )

We need a IP that the signer can listen on, and the baker can connect to, to do this we create a dummy interface:

```
$ ip link add dummy0 type dummy
$ ip addr add 192.168.1.150/0 dev dummy0
$ ifconfig dummy0 up
```

Now we have created a dummy IP 192.168.1.150 that we can use to communicate between containers.

## Start the sandbox

We start a new Sandbox, and remove the bootstrap5 address from the local client,
because this is the address we will configure in the remote-signer.

```
$ teztool create mysandbox --network sandbox --rpc-port 58732
$ teztool mysandbox client "forget address bootstrap5 --force"
```



## Start the Signer

We start the signer, listening to the created IP, with: 

```
$ teztool signer create mysigner 192.168.1.150 23000
Signer created!
Configure Addresses with `signer mysigner shell`,
and then start the signer.

```

Then create the key in the signer container and start it:

```
$ teztool signer mysigner shell
Creating Signer Shell Container...

/mounted $ tezos-client import secret key baker unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm
Tezos address added: tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv
/mounted $ exit

$ teztool signer mysigner start
```

## Import the Key

```
$ teztool mysandbox client import secret key remoteSigner tcp://192.168.1.150:23000/tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv
Tezos address added: tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv
```

Now we can bake one block for testing:

```
$ teztool mysandbox client bake for remoteSigner
Feb 23 18:46:29 - 005-PsBabyM1.baking.forge: found 0 valid operations (0 refused) for timestamp 2020-02-23T18:46:29-00:00 (fitness 01::0000000000000001)
Injected block BMCbFGA3Hk9z
```

And check the logs in the Signer:

```
$ teztool signer mysigner logs
Tezos address added: tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv
Feb 23 18:43:05 - client.signer: Accepting TCP requests on 0.0.0.0:22000
Feb 23 18:45:17 - client.signer: Request for public key tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv
Feb 23 18:45:17 - client.signer: Found public key for hash tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv (name: baker)
Feb 23 18:46:29 - client.signer: Request for signing 147 bytes of data for key tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv, magic byte = 01
Feb 23 18:46:29 - client.signer: Signing data for key baker
```

Now that we know everything works, we can start the baker containers:

```
$ teztool mysandbox baker start remoteSigner
 
$ teztool mysandbox baker logs
Waiting for the node to be synchronized with its peers...
Node synchronized.
Baker started.
Feb 23 18:47:34 - 005-PsBabyM1.baking.forge: New baking slot found (level 3, priority 10) at 2020-02-23T18:47:01-00:00 for remoteSigner after BMCbFGA3Hk9z.
....
```



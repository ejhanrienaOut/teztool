package internal

import (
	"fmt"
	"os"
	"time"
)

func must(step string, e error) {
	if e != nil {
		fmt.Println("Error when " + step + ":")
		fmt.Println(e)
		os.Exit(0)
	}
}

type SnapshotInfos struct {
	Snapshots  map[string]Snapshot
	LastChange time.Time
}

type Snapshot struct {
	Filename  string
	Network   string
	Block     uint
	Blockhash string
	Blocktime time.Time
}

type BlockHeader struct {
	Protocol         string    `json:"protocol"`
	ChainID          string    `json:"chain_id"`
	Hash             string    `json:"hash"`
	Level            uint      `json:"level"`
	Proto            int       `json:"proto"`
	Predecessor      string    `json:"predecessor"`
	Timestamp        time.Time `json:"timestamp"`
	ValidationPass   int       `json:"validation_pass"`
	OperationsHash   string    `json:"operations_hash"`
	Fitness          []string  `json:"fitness"`
	Context          string    `json:"context"`
	Priority         int       `json:"priority"`
	ProofOfWorkNonce string    `json:"proof_of_work_nonce"`
	Signature        string    `json:"signature"`
}

package internal

import (
	"encoding/json"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/nomadic-labs/teztool/util"
	"strings"
	"time"
)

func GetDefaultTemplate() DockerTemplate {
	new := DockerTemplate{}
	new.PortMapping = map[uint]uint{}
	new.PortMappingUDP = map[uint]uint{}
	new.Binds = map[string]string{}
	new.Resources = Resources{
		4000,        // max 4 CPU
		16000000000, // max 16 GB
	}
	new.Hostname = "container"
	new.Labels = map[string]string{
		"created_by":    "teztool",
		"creation_date": time.Now().String(),
	}
	new.DNS = []string{"8.8.8.8", "8.8.4.4"}

	return new
}

func GetTezosTemplate(config Tezos) DockerTemplate {
	t := GetDefaultTemplate()
	t.NetworkAliases = []string{"tezos", "node", "tezosnode"}
	t.Image = GetTezosImage()
	t.EnvVariables = map[string]string{
		"DATA_DIR": "/home/tezos/.tezos-node/",
	}
	t.NetworkId = config.DockerNetwork

	t.Mounts = map[string]string{
		config.DataVolume:   "/home/tezos/.tezos-node/",
		config.ClientVolume: "/home/tezos/.tezos-client",
	}

	t.Command = strslice.StrSlice{"/usr/local/bin/tezos-node", "run",
		"--data-dir", "/home/tezos/.tezos-node/",
		"--rpc-addr", ":8732"}

	if len(config.RunFlags) >= 1 {
		t.Command = append(t.Command, strings.Split(config.RunFlags, " ")...)
	}
	t.Entrypoint = strslice.StrSlice{""}

	t.Labels["network"] = config.Network
	t.Labels["node"] = "true"
	t.Labels["node_name"] = config.Name
	confbytes, _ := json.Marshal(config)
	t.Labels["teztool_config"] = string(confbytes)

	t.ContainerName = "teztool_" + config.Name

	if config.P2pPort != 0 {
		t.PortMappingUDP = map[uint]uint{
			config.P2pPort: 9732,
		}
	} else {
		if val := GetDataPortDEPRECTATED(config.Network); val != 0 {
			t.PortMappingUDP = map[uint]uint{
				val: 9732,
			}
		}
	}

	if config.RpcPort != 0 {
		t.PortMapping = map[uint]uint{
			config.RpcPort: 8732,
		}
	}

	if config.ExposeRPC {
		t.ExposeIP = "0.0.0.0"
	} else {
		t.ExposeIP = "127.0.0.1"
	}

	return t
}

func GetSandboxTemplate(config Tezos) DockerTemplate {
	t := GetDefaultTemplate()
	t.NetworkAliases = []string{"tezos", "node", "tezosnode"}
	t.Image = GetTezosImage()
	t.EnvVariables = map[string]string{
		"DATA_DIR":    "/home/tezos/.tezos-sandbox",
		"SANDBOXJSON": string(GetSandboxJson()),
	}
	t.NetworkId = config.DockerNetwork

	t.Mounts = map[string]string{
		config.DataVolume:   "/home/tezos/.tezos-node/",
		config.ClientVolume: "/home/tezos/.tezos-client/",
	}

	t.Command = strslice.StrSlice{"/usr/local/bin/tezos-node", "run",
		"--cors-origin=*",
		"--singleprocess",
		"--rpc-addr", "0.0.0.0:8732",
		"--private-mode",
		"--bootstrap-threshold=0",
		"--connections=0",
		"--data-dir", "/home/tezos/.tezos-node",
		"--no-bootstrap-peers",
		"--sandbox=/home/tezos/sandbox.json"}

	if len(config.RunFlags) >= 1 {
		t.Command = append(t.Command, strings.Split(config.RunFlags, " ")...)
	}
	t.Entrypoint = strslice.StrSlice{""}

	t.Labels["network"] = config.Name
	t.Labels["node_name"] = config.Name

	confbytes, _ := json.Marshal(config)
	t.Labels["teztool_config"] = string(confbytes)

	t.ContainerName = "teztool_" + strings.ToLower(config.Name)
	t.ExposeIP = "127.0.0.1"

	if config.RpcPort != 0 {
		t.PortMapping = map[uint]uint{
			config.RpcPort: 8732,
		}
	}

	return t
}

func GetTezosTask(config Tezos, command string) DockerTemplate {
	t := GetDefaultTemplate()
	t.Rm = true
	t.NetworkAliases = []string{"tezos", "node", "tezosnode"}
	t.Image = GetTezosImage()

	t.EnvVariables = map[string]string{
		"DATA_DIR": "/home/tezos/.tezos-node/",
	}

	t.Mounts = map[string]string{
		config.DataVolume:   "/home/tezos/.tezos-node/",
		config.ClientVolume: "/home/tezos/.tezos-client",
	}

	t.Command = strslice.StrSlice{command}
	t.Entrypoint = strslice.StrSlice{"sh", "-c"}

	t.ContainerName = "teztool_" + config.Network + "_task_" + util.RandomString(5)

	return t
}

// bin: baker, endorser
// proto: 005-PsBabyM1
// proto: 006-PsCARTHA
func GetBakerTemplate(config Tezos, proto string, binary string) DockerTemplate {
	template := GetTezosTemplate(config)
	template.Mounts = map[string]string{
		config.DataVolume:   "/home/tezos/.tezos-node/",
		config.ClientVolume: "/root/.tezos-client",
	}
	template.ContainerName = "teztool_" + config.Name + "_" + binary + "_" + proto
	template.Labels["baker"] = "true"
	template.Labels["proto"] = proto
	template.Hostname = "baker"
	template.NetworkAliases = []string{binary + "_ " + proto}
	template.PortMapping = map[uint]uint{}
	template.PortMappingUDP = map[uint]uint{}
	bake_append := " with local node /home/tezos/.tezos-node --max-priority 128"
	if binary != "baker" {
		bake_append = ""
	}
	template.Command = strings.Split("tezos-"+binary+"-"+proto+" -A node run"+bake_append, " ")
	return template
}

func GetSignerTemplate(name, ip string, port int, vol string) DockerTemplate {
	t := GetDefaultTemplate()
	t.NetworkAliases = []string{"tezos", "node", "tezosnode"}
	t.Image = GetTezosImage()
	t.Labels["teztool_signer"] = "true"
	t.Labels["teztool_signer_name"] = name
	t.EnvVariables = map[string]string{
		"DATA_DIR": "/home/tezos/.tezos-node/",
	}

	t.Mounts = map[string]string{
		vol: "/root/.tezos-client",
	}

	t.ContainerName = "teztool_signer_" + name

	t.PortMapping = map[uint]uint{
		uint(port): 22000,
	}

	if ip != "" {
		t.ExposeIP = "0.0.0.0"
	} else {
		t.ExposeIP = "127.0.0.1"
	}
	t.Command = []string{"sh", "-c", "tezos-signer launch socket signer -a 0.0.0.0 -p 22000"}
	return t
}

func GetCopierTemplate(from, to string) DockerTemplate {
	template := GetDefaultTemplate()
	template.Image = "docker.io/library/alpine:latest"
	template.Mounts = map[string]string{
		from: "/sourcevol",
		to:   "/targetvol",
	}
	template.ContainerName = "teztool_copier_" + util.RandomString(5)
	template.Command = []string{"sh", "-c", GetCopierScript()}
	return template
}

func GetImporterTemplate(from, to string) DockerTemplate {
	template := GetDefaultTemplate()
	template.Image = "docker.io/library/alpine:latest"
	template.Mounts = map[string]string{
		to: "/targetvol",
	}
	template.Binds = map[string]string{
		from: "/importsrc",
	}
	template.ContainerName = "teztool_copier_" + util.RandomString(5)
	template.Command = []string{"sh", "-c", GetImporterScript()}
	return template
}

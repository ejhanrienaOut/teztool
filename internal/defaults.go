package internal

import "os"

func GetProtocols() []string {
	return []string{
		"ProtoGenesis",
		"ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
		"Ps6mwMrF2ER2s51cp9yYpjDcuzQjsc2yAz8bQsRgdaRxw4Fk95H",
		"Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P",
		"PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU",
		"PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS",
		"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb",
		"PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt",
		"PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP",
		"Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd",
		"PtBMwNZT94N7gXKw4i273CKcSaBrrBnqnt3RATExNKr9KNX2USV",
		"PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY",
		"PtYuensgYBb3G3x1hLLbCmcav8ue8Kyd2khADcL5LsT5R1hcXex",
	}
}

func GetSandboxJson() []byte {
	return []byte(`{ "genesis_pubkey": "edpkuSLWfVU1Vq7Jg9FucPyKmma6otcMHac9zG4oU1KMHSTBpJuGQ2"}`)
}

func GetEmptyConfig() string {
	return `{ "p2p":{ "bootstrap-peers": [ "boot.tzbeta.net" ], "listen-addr": "[::]:9732" } }`
}

func GetDataPortDEPRECTATED(net string) uint {
	ports := map[string]uint{
		"mainnet":     9732,
		"carthagenet": 19732,
		"zeronet":     9730,
	}
	if val, ok := ports[net]; ok {
		return val
	}
	return 0
}

func GetTezosImage() string {
	tag := "master_e24f29e9_20200414134543"
	if os.Getenv("TEZTOOL_TEZOS_TAG") != "" {
		tag = os.Getenv("TEZTOOL_TEZOS_TAG")
	}
	return "docker.io/tezos/tezos-bare:" + tag
}

package internal

type Resources struct {
	CPUCount  uint // in milliCPU
	MemoryMax uint // in byte
}

type DockerTemplate struct {
	Rm             bool
	Image          string
	ContainerName  string
	Hostname       string
	NetworkAliases []string
	Entrypoint     []string
	Command        []string
	Mounts         map[string]string
	Binds          map[string]string
	Resources      Resources
	EnvVariables   map[string]string
	Labels         map[string]string
	DNS            []string
	PortMapping    map[uint]uint
	PortMappingUDP map[uint]uint
	NetworkId      string
	ExposeIP       string
}

type Tezos struct {
	Name            string
	RunFlags        string
	SandboxProto    string
	Config          []byte
	Network         string
	DataVolume      string
	ClientVolume    string
	HistoryMode     string
	Status          string // possible values: running / deleted / paused
	RpcPort         uint   // host port
	P2pPort         uint   // p2p (udp) port
	RpcIP           string // what interface to bind on
	DockerNetwork   string
	SandboxProtocol string
	SandboxParams   string
	ExposeRPC       bool
}

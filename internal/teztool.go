package internal

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/docker/docker/api/types/strslice"
	"gitlab.com/nomadic-labs/teztool/util"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type TezTool struct {
	docker *DockerWrapper
}

func NewTezTool(d *DockerWrapper) *TezTool {
	t := TezTool{}
	t.docker = d

	return &t
}

// Get docker network
func (this *TezTool) GetNetwork(nodename string) (string, string) {
	name := "teztool_" + nodename
	id, err := this.docker.GetNetwork(name)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return name, id
}

// Get data volume
func (this *TezTool) GetDataVolume(nodename string) (string, error) {
	name := "teztool_" + nodename + "_data"
	name, err := this.docker.GetVolume(name, nodename)
	return name, err
}

// Copy between volumes
func (this *TezTool) CopyVolumes(from, to string) ([]byte, error) {
	return this.docker.CopyDataVol(from, to)
}

// Import Folder or Tar
func (this *TezTool) Import(from, to string) ([]byte, error) {
	return this.docker.Import(from, to)
}

func (this *TezTool) PullImage() {
	_, _ = this.docker.PullImageBlocking(GetTezosImage())
}

func (this *TezTool) PullCustomImage(image string) {
	_, _ = this.docker.PullImageBlocking(image)
}

// Get client volume
func (this *TezTool) GetClientVolume(nodename string) (string, error) {
	name := "teztool_" + nodename + "_client"
	name, err := this.docker.GetVolume(name, nodename)
	return name, err
}

func (this *TezTool) GetConfigBytes() []byte {
	confstr :=
		`{ "p2p":
	  {
	    "listen-addr": "[::]:9732"
	  }
	}`
	return []byte(confstr)
}

func (this *TezTool) WriteVersionFile(t Tezos) error {
	idgen := GetTezosTask(t,
		"/usr/local/bin/tezos-node config init && cp /root/.tezos-node/version.json /home/tezos/.tezos-node//version.json")
	_, err := this.docker.RunRM(idgen)
	return err
}

func (this *TezTool) GenerateIdentity(t Tezos) error {
	idgen := GetTezosTask(t,
		"/usr/local/bin/tezos-node identity generate --data-dir /home/tezos/.tezos-node/")
	_, err := this.docker.RunRMMany(idgen, 4)
	return err
}

func (this *TezTool) GenerateDummyIdentity(t Tezos) error {
	idgen := GetTezosTask(t,
		"/usr/local/bin/tezos-node identity generate 2 --data-dir /home/tezos/.tezos-node/")
	_, err := this.docker.RunRM(idgen)
	return err
}

func (this *TezTool) StartTezos(t Tezos) (string, error) {
	template := GetTezosTemplate(t)
	id, err := this.docker.GetContainerId(template.ContainerName)
	if err != nil || id == "" {
		id, err = this.docker.ContainerCreate(template)
	}
	this.docker.StartContainer(id)
	return id, err
}

func (this *TezTool) StartSandbox(t Tezos) (string, error) {
	template := GetSandboxTemplate(t)
	id, err := this.docker.GetContainerId(template.ContainerName)
	if err != nil || id == "" {
		id, err = this.docker.ContainerCreate(template)
	}

	_ = ioutil.WriteFile("/tmp/sandbox.json", GetSandboxJson(), 0777)
	cmd := exec.Command("docker", "cp", "/tmp/sandbox.json", template.ContainerName+":/home/tezos/sandbox.json")
	_ = cmd.Run()

	if t.SandboxProtocol == "ProtoGenesis" {
		return id, nil
	}

	this.docker.StartContainer(id)
	time.Sleep(1 * time.Second)

	// copy file
	_ = ioutil.WriteFile("/tmp/sandbox_parameters.json", []byte(t.SandboxParams), 0777)
	cmd = exec.Command("docker", "cp", "/tmp/sandbox_parameters.json", template.ContainerName+":/home/tezos/sandbox_parameters.json")
	_ = cmd.Run()

	cmds := []string{
		"tezos-client import secret key activator unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6",
		"tezos-client -b genesis activate protocol " + t.SandboxProto + " with fitness 1 and key activator and parameters /home/tezos/sandbox_parameters.json",
		"tezos-client import secret key bootstrap1 unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh",
		"tezos-client import secret key bootstrap2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo",
		"tezos-client import secret key bootstrap3 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ",
		"tezos-client import secret key bootstrap4 unencrypted:edsk2uqQB9AY4FvioK2YMdfmyMrer5R8mGFyuaLLFfSRo8EoyNdht3",
		"tezos-client import secret key bootstrap5 unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm",
		"cp -ra /root/.tezos-client/. /home/tezos/.tezos-client",
		"ls /home/tezos/.tezos-client",
		"chmod -cR 777 /home/tezos/.tezos-client",
	}

	for _, cmd := range cmds {
		r, _, _ := this.docker.ExecRWID(id,
			strings.Split(cmd, " "),
		)
		_, _ = ioutil.ReadAll(r)
		// wait to finish

	}

	return id, err
}

func (this *TezTool) WriteIntoVolume(content []byte, path, filename string, volume string) error {
	image := "docker.io/library/alpine:20200428"
	copy := GetDefaultTemplate()
	this.PullCustomImage(image)

	copy.Mounts = map[string]string{
		volume: "/target",
	}

	copy.EnvVariables = map[string]string{
		"FILE": string(content),
	}

	copy.Image = image
	cmd := "mkdir -p /target" + path + " && chmod 777 /target" + path + " && echo $FILE > /target" + path + "/" + filename + " && sleep 1000"
	copy.Command = []string{"sh", "-c", cmd}

	id, err := this.docker.ContainerCreate(copy)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	this.docker.StartContainer(id)
	time.Sleep(500 * time.Millisecond)
	this.docker.DeleteContainer(id)

	return nil
}

func (this *TezTool) StopContainers(name string) error {
	containers, err := this.docker.FindContainersForName(name)
	if err != nil {
		return err
	}
	for _, c := range containers {
		if c.State == "running" {
			fmt.Println("Stopping Container: ", c.Names[0])
			this.docker.StopContainer(c.ID)
		}
	}
	return err
}

func (this *TezTool) StartShell(t Tezos) error {
	this.PullImage()
	template := GetTezosTemplate(t)
	template.Labels["node"] = "false"
	template.NetworkAliases = []string{"shell"}
	template.PortMappingUDP = map[uint]uint{}
	template.PortMapping = map[uint]uint{}
	dir, _ := os.Getwd()
	if os.Getenv("DIND_PWD") != "" {
		dir = os.Getenv("DIND_PWD")
	}
	template.Binds[dir] = "/mounted"
	template.ContainerName += "_shell_" + util.RandomString(6)
	template.Command = strslice.StrSlice{"tail", "-f", "/dev/null"}
	id, err := this.docker.GetContainerId(template.ContainerName)
	if err != nil || id == "" {
		id, err = this.docker.ContainerCreate(template)
	}
	this.docker.StartContainer(id)

	cmd := exec.Command("docker", "exec", "-u", "tezos", "-it", id, "sh", "-c", "cd /mounted && sudo chmod -cR 777 /home/tezos/.tezos-client/ > /dev/null 2>&1 && tezos-client -A node config update > /dev/null 2>&1 && sh")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdout

	_ = cmd.Run()

	this.docker.DeleteContainer(id)

	return err
}

func (this *TezTool) StartSignerShell(vol string) error {
	this.PullImage()
	template := GetSignerTemplate("shell", "", 0, vol)
	template.NetworkAliases = []string{"shell"}
	template.PortMappingUDP = map[uint]uint{}
	template.PortMapping = map[uint]uint{}
	dir, _ := os.Getwd()
	if os.Getenv("DIND_PWD") != "" {
		dir = os.Getenv("DIND_PWD")
	}
	template.Binds[dir] = "/mounted"
	template.ContainerName += "_shell_" + util.RandomString(6)
	template.Command = strslice.StrSlice{"tail", "-f", "/dev/null"}
	id, err := this.docker.GetContainerId(template.ContainerName)
	if err != nil || id == "" {
		id, err = this.docker.ContainerCreate(template)
	}
	this.docker.StartContainer(id)

	cmd := exec.Command("docker", "exec", "-u", "root", "-it", id, "sh", "-c", "cd /mounted && sh")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdout

	_ = cmd.Run()

	this.docker.DeleteContainer(id)

	return err
}

func (this *TezTool) StartShellWithCommand(t Tezos, command string) error {
	this.PullImage()
	template := GetTezosTemplate(t)
	template.Labels["node"] = "false"
	template.PortMappingUDP = map[uint]uint{}
	template.PortMapping = map[uint]uint{}
	template.NetworkAliases = []string{"shell"}
	dir, _ := os.Getwd()
	if os.Getenv("DIND_PWD") != "" {
		dir = os.Getenv("DIND_PWD")
	}
	template.Binds[dir] = "/mounted"
	template.ContainerName += "_shell_" + util.RandomString(6)
	template.Command = strslice.StrSlice{"tail", "-f", "/dev/null"}
	id, err := this.docker.GetContainerId(template.ContainerName)
	if err != nil || id == "" {
		id, err = this.docker.ContainerCreate(template)
	}
	this.docker.StartContainer(id)

	cmd := exec.Command("docker", "exec", "-u", "tezos", "-it", id, "sh", "-c", "cd /mounted && "+command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdout

	_ = cmd.Run()

	this.docker.DeleteContainer(id)

	return err
}

func (this *TezTool) RunShell(t Tezos, cmds []string) (error, int) {
	this.PullImage()
	template := GetTezosTemplate(t)
	template.Labels["node"] = "false"
	template.PortMapping = map[uint]uint{}
	template.PortMappingUDP = map[uint]uint{}
	dir, _ := os.Getwd()
	if os.Getenv("DIND_PWD") != "" {
		dir = os.Getenv("DIND_PWD")
	}
	template.Binds[dir] = "/mounted"
	template.ContainerName += "_shell_" + util.RandomString(6)
	template.Command = strslice.StrSlice{"tail", "-f", "/dev/null"}
	id, err := this.docker.GetContainerId(template.ContainerName)
	if err != nil || id == "" {
		id, err = this.docker.ContainerCreate(template)
	}
	this.docker.StartContainer(id)
	defer this.docker.DeleteContainer(id)

	code := 0 // successfull

	for _, c := range cmds {
		cmd := exec.Command("docker", "exec", "-u", "tezos", id, "sh", "-c", c)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		_ = cmd.Run()
		code = cmd.ProcessState.ExitCode()
		if code != 0 {
			return errors.New("Command returned non-0 exit code"), code
		}
	}
	return err, code
}

func (this *TezTool) StartBaker(t Tezos, protocol string) error {
	baker := GetBakerTemplate(t, protocol, "baker")
	endorser := GetBakerTemplate(t, protocol, "endorser")
	templates := []DockerTemplate{baker, endorser}
	for _, template := range templates {
		id, err := this.docker.GetContainerId(template.ContainerName)
		if err != nil || id == "" {
			id, err = this.docker.ContainerCreate(template)
		}
		this.docker.StartContainer(id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *TezTool) CreateSigner(name string, port string, ip string) error {
	portInt, err := strconv.Atoi(port)
	if err != nil {
		fmt.Println(err)
		return errors.New("Error: invalid Port!")
	}

	vol, err := this.GetClientVolume("signer_" + name)
	if err != nil {
		return errors.New("Error creating signer volume")
	}
	s := GetSignerTemplate(name, ip, portInt, vol)
	_, err = this.docker.ContainerCreate(s)
	if err != nil {
		return errors.New("Error creating Signer Container")
	}
	return nil
}

func (this *TezTool) StartSandboxBaker(t Tezos, protocol string, address string) error {
	if protocol == "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb" {
		protocol = "006-PsCARTHA"
	}
	baker := GetBakerTemplate(t, protocol, "baker")
	endorser := GetBakerTemplate(t, protocol, "endorser")
	baker.Command = append(baker.Command, address)
	endorser.Command = append(baker.Command, address)
	templates := []DockerTemplate{baker, endorser}
	for _, template := range templates {
		id, err := this.docker.GetContainerId(template.ContainerName)
		if err != nil || id == "" {
			id, err = this.docker.ContainerCreate(template)
		}
		this.docker.StartContainer(id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (this *TezTool) GetTezos(name string) (Tezos, error) {
	res := Tezos{}

	container_name := "teztool_" + name

	containerID, err := this.docker.GetContainerId(container_name)
	if err != nil {
		return res, err
	}
	containerInfo, err := this.docker.GetContainerInfo(containerID)
	if err != nil {
		return res, err
	}

	// config is stored as label in the container conf
	_ = json.Unmarshal([]byte(containerInfo.Config.Labels["teztool_config"]), &res)

	return res, err
}

func (this *TezTool) PurgeVolumes(network string) error {
	volumes, err := this.docker.FindVolumesForName(network)
	if err != nil {
		return err
	}
	for _, c := range volumes {
		fmt.Println("Deleting Volume: ", c.Name)
		this.docker.DeleteVolume(c.Name)
	}
	return err
}

func (this *TezTool) RemoveBakerContainers(name string, proto string) error {
	containers, err := this.docker.FindContainersForName(name)
	if err != nil {
		return err
	}
	for _, c := range containers {
		if val, ok := c.Labels["baker"]; !ok || val != "true" {
			continue
		}
		if val, ok := c.Labels["proto"]; !ok || val != proto {
			continue
		}
		fmt.Println("Removing Container: ", c.Names[0])
		this.docker.DeleteContainer(c.ID)

	}
	return err
}

func (this *TezTool) RemoveSignerContainer(name string) error {
	containers, err := this.docker.FindContainers()
	if err != nil {
		return err
	}
	for _, c := range containers {
		if val, ok := c.Labels["teztool_signer"]; !ok || val != "true" {
			continue
		}
		if val, ok := c.Labels["teztool_signer_name"]; !ok || val != name {
			continue
		}
		fmt.Println("Removing Container: ", c.Names[0])
		this.docker.DeleteContainer(c.ID)
	}
	return err
}

func (this *TezTool) StartSignerContainer(name string) error {
	containers, err := this.docker.FindContainers()
	if err != nil {
		return err
	}
	for _, c := range containers {
		if val, ok := c.Labels["teztool_signer"]; !ok || val != "true" {
			continue
		}
		if val, ok := c.Labels["teztool_signer_name"]; !ok || val != name {
			continue
		}
		fmt.Println("Starting Container: ", c.Names[0])
		this.docker.StartContainer(c.ID)
	}
	return err
}

func (this *TezTool) StopSignerContainer(name string) error {
	containers, err := this.docker.FindContainers()
	if err != nil {
		return err
	}
	for _, c := range containers {
		if val, ok := c.Labels["teztool_signer"]; !ok || val != "true" {
			continue
		}
		if val, ok := c.Labels["teztool_signer_name"]; !ok || val != name {
			continue
		}
		fmt.Println("Stopping Container: ", c.Names[0])
		this.docker.StopContainer(c.ID)
	}
	return err
}

func (this *TezTool) RemoveContainers(name string) error {
	containers, err := this.docker.FindContainersForName(name)
	if err != nil {
		return err
	}
	for _, c := range containers {
		fmt.Println("Removing Container: ", c.Names[0])
		this.docker.DeleteContainer(c.ID)

	}
	return err
}

func (this *TezTool) RemoveContainer(name string) error {
	id, err := this.docker.GetContainerId(name)
	if err != nil {
		return err
	}
	this.docker.StopContainer(id)
	this.docker.DeleteContainer(id)
	return err
}

func (this *TezTool) GetRunningNetworks() error {
	containers, err := this.docker.FindContainers()
	if err != nil {
		return err
	}
	for _, c := range containers {
		if c.State == "running" {
			fmt.Println("Stopping Container: ", c.Names[0][1:])
			this.docker.StopContainer(c.ID)
		}
	}
	return err
}

func (this *TezTool) Logs(containername string) {
	id, err := this.docker.GetContainerId(containername)
	if err != nil {
		fmt.Println("no such container!")
		os.Exit(1)
	}
	logs, _ := this.docker.GetLogs(id, true, "500")
	s := bufio.NewScanner(logs)
	for s.Scan() {
		fmt.Println(string(s.Bytes()[8:]))
	}
}

func (this *TezTool) LogStream(containername string) chan string {
	res := make(chan string)
	id, err := this.docker.GetContainerId(containername)
	if err != nil {
		fmt.Println("no such container!")
		os.Exit(1)
	}
	logs, _ := this.docker.GetLogs(id, true, "500")
	s := bufio.NewScanner(logs)
	go func() {
		for s.Scan() {
			res <- string(s.Bytes()[8:])
		}
	}()
	return res
}

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"os"
)

func GetDeleteDataCommand(c *dig.Container) *cobra.Command {
	var delete = &cobra.Command{
		Use:   "delete [volume]",
		Short: "Delete a single data volume",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				vols, err := docker.FindVolumes()

				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}

				if len(vols) == 0 {
					fmt.Println("Cant find any Volumes")
					os.Exit(1)
				}

				items := []string{}
				for _, v := range vols {
					items = append(items, v.Name)
				}

				res := GetArg(args, 0, "Select Volume;", items)
				docker.DeleteVolume(res)
			})

		},
	}

	return delete
}

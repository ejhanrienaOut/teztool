package cmd

import (
	"errors"
	"fmt"
	"github.com/manifoldco/promptui"
	"os"
	"strconv"
)

func MustExist(key string, array []string) string {
	for _, a := range array {
		if key == a {
			return a
		}
	}
	fmt.Println("Invalid Input: " + key)
	if len(array) == 1 {
		fmt.Println("Currently no valid Options...")
		return ""
	}
	fmt.Println("Options are: ")
	for _, o := range array {
		if len(o) != 0 {
			fmt.Println("\t*", o)
		}
	}
	os.Exit(1)
	return ""
}

func MustNotExist(key string, array []string) string {
	for _, a := range array {
		if key == a {
			fmt.Println("Invalid input,", key, "already exists")
		}
	}
	return key
}

func GetArg(args []string, offset int, question string, items []string) string {
	if offset < len(args) {
		return MustExist(args[offset], items)
	}

	prompt := promptui.Select{
		Label: question,
		Items: items,
	}
	_, choice, err := prompt.Run()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return choice
}

func GetStringExcept(args []string, offset int, question string, items []string) string {
	if len(args) > offset {
		if checked := MustNotExist(args[offset], items); checked != "" {
			return checked
		}
	}

	validate := func(input string) error {
		if len(input) <= 3 {
			return errors.New("too short")
		}
		for _, a := range items {
			if a == input {
				return errors.New(a + " already exists")
			}
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    question,
		Validate: validate,
	}

	choice, err := prompt.Run()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return choice
}

func GetNumber(args []string, offset int, question string) uint {
	if offset < len(args) {
		intResult, _ := strconv.ParseInt(args[offset], 10, 64)
		return uint(intResult)
	}

	validate := func(input string) error {
		_, err := strconv.ParseInt(input, 10, 64)
		if err != nil {
			return errors.New("Invalid number")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    question,
		Validate: validate,
	}
	result, err := prompt.Run()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	intResult, _ := strconv.ParseInt(result, 10, 64)
	return uint(intResult)
}

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"os"
	"os/signal"
	"time"
)

func GetBakeCommand(c *dig.Container, name string) *cobra.Command {
	var bake = &cobra.Command{
		Use:   "baker",
		Short: "Manage Baker",
	}
	bake.AddCommand(GetBakerStartCommand(c, name))
	bake.AddCommand(GetBakeStopCommand(c, name))
	bake.AddCommand(GetBakerLogsCommand(c, name))
	return bake
}

func GetBakerStartCommand(c *dig.Container, name string) *cobra.Command {
	var bake = &cobra.Command{
		Use:   "start [protocol]",
		Short: "Start a Baker for a Network/Protocol",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				proto := GetArg(args, 0, "Select Protocol", []string{"006-PsCARTHA"})

				tez, err := tool.GetTezos(name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				e := tool.StartBaker(tez, proto)
				if e != nil {
					fmt.Println(e)
				}
			})
		},
	}

	return bake
}

func GetBakeStopCommand(c *dig.Container, name string) *cobra.Command {
	var bake = &cobra.Command{
		Use:   "stop",
		Short: "Stop a Baker for a Network/Protocol",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				proto := GetArg(args, 0, "Select Protocol", []string{"006-PsCARTHA", "005-PsBabyM1"})

				e := tool.RemoveBakerContainers(name, proto)
				if e != nil {
					fmt.Println(e)
				}

			})
		},
	}

	return bake
}

func GetBakerLogsCommand(c *dig.Container, name string) *cobra.Command {
	var logs = &cobra.Command{
		Use:   "logs ",
		Short: "Show tezos Logs",
		Run: func(cmd *cobra.Command, args []string) {

			time.Sleep(10 * time.Millisecond)
			e := c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				cs, _ := docker.FindContainersForName(name)
				for _, c := range cs {
					if _, ok := c.Labels["baker"]; ok {
						stream := tool.LogStream(c.Names[0][1:])
						go func(stream chan string) {
							for {
								log := <-stream
								fmt.Println(log)
							}
						}(stream)
					}
				}

				var signal_channel chan os.Signal
				signal_channel = make(chan os.Signal, 1)
				signal.Notify(signal_channel, os.Interrupt)
				<-signal_channel

			})

			if e != nil {
				panic(e)
			}

		},
	}

	return logs
}

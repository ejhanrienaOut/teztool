package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"time"
)

func GetLogsCommand(c *dig.Container, name string) *cobra.Command {
	var logs = &cobra.Command{
		Use:   "logs ",
		Short: "Show Logs",
		Run: func(cmd *cobra.Command, args []string) {

			time.Sleep(10 * time.Millisecond)
			e := c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				tool.Logs("teztool_" + name)
			})

			if e != nil {
				panic(e)
			}

		},
	}

	return logs
}

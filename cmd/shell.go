package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"os"
	"time"
)

func GetShellCommand(c *dig.Container, name string) *cobra.Command {
	var shell = &cobra.Command{
		Use:   "shell",
		Short: "Open a Shell inside a Tezos Container",
		Run: func(cmd *cobra.Command, args []string) {
			t := internal.Tezos{}

			// create node
			fmt.Println("Creating Tezos Shell Container...\n")
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				t.Name = name

				// Create network
				id, _ := tool.GetNetwork(t.Name)
				t.DockerNetwork = id
				// Create Data Volume
				datavol, err := tool.GetDataVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.DataVolume = datavol

				// Create Client Volume
				clientvol, err := tool.GetClientVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.ClientVolume = clientvol

				tool.StartShell(t)
				if err != nil {
					fmt.Println(err.Error())
					os.Exit(1)
				}

			})
		},
	}

	return shell
}

func GetWrapperCommand(c *dig.Container, nodeName string, name, description, binary string) *cobra.Command {
	var shell = &cobra.Command{
		Use:   name,
		Short: description,
		Run: func(cmd *cobra.Command, args []string) {
			t := internal.Tezos{}

			cmdString := ""
			for _, a := range args {
				cmdString += " " + a
			}
			// create node
			time.Sleep(10 * time.Millisecond)
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				t.Name = nodeName

				// Create network
				id, _ := tool.GetNetwork(t.Name)
				t.DockerNetwork = id
				t.DockerNetwork = id
				// Create Data Volume
				datavol, err := tool.GetDataVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.DataVolume = datavol

				// Create Client Volume
				clientvol, err := tool.GetClientVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.ClientVolume = clientvol
				tool.StartShellWithCommand(t, binary+cmdString)
				if err != nil {
					fmt.Println(err.Error())
					os.Exit(1)
				}

			})
		},
	}

	return shell
}

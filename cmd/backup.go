package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"os"
	"strings"
)

func GetBackupCommand(c *dig.Container, net string) *cobra.Command {
	var snap = &cobra.Command{
		Use:   "backup",
		Short: "Create or restore a Backup file",
	}

	snap.AddCommand(GetBackupCreateCommand(c, net))
	snap.AddCommand(GetBackupRestoreCommand(c, net))
	return snap
}

func GetBackupCreateCommand(c *dig.Container, name string) *cobra.Command {
	var snap = &cobra.Command{
		Use:   "create [./file]",
		Short: "Create a backup .tar.gz file",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Usage: backup create [file]")
				return
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				t := docker.FindNode(name)

				// Create network
				id, _ := tool.GetNetwork(t.Name)
				t.DockerNetwork = id
				// Create Data Volume
				datavol, err := tool.GetDataVolume(t.Name)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}
				t.DataVolume = datavol
				fmt.Println("Stopping Tezos Node and related Containers")
				tool.StopContainers(t.Name)

				fmt.Println("Starting helper container....")

				err, code := tool.RunShell(*t, []string{
					"echo 'preparing Data directory....'",
					"sudo touch " + args[0] + " && sudo chmod 777 " + args[0],
					"sudo chmod -cR 777 /home/tezos/.tezos-node > /dev/null 2>&1",
					"echo 'Starting Backup...'",
					"sudo tar -czvf /mounted/" + args[0] + " /home/tezos/.tezos-node",
				})

				if err != nil {
					fmt.Println(err.Error())
					os.Exit(code)
				}

				fmt.Println("Export done\n")

			})

		},
	}

	return snap
}

func GetBackupRestoreCommand(c *dig.Container, name string) *cobra.Command {
	import_clientdir := true
	var snap = &cobra.Command{
		Use:   "restore [mode] [./file]",
		Short: "restore a backup file",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Usage: backup import [./file]")
				return
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				t := docker.FindNode(name)
				// check if docker volume
				list, err := docker.GetVolumes()

				for _, l := range list {
					if l == args[0] {
						err = ImportFromVolume(*t, tool, l)
						if err != nil {
							fmt.Println(err.Error())
							os.Exit(1)
						}
						return
					}
				}

				// imports folder or .tar.gz archive

				path := args[0]
				if path[:1] == "/" {
					// path is absolute, do nothing
				} else {
					// need some tricks because maybe this executes inside a docker
					dir, _ := os.Getwd()
					if os.Getenv("DIND_PWD") != "" {
						dir = os.Getenv("DIND_PWD") + "/"
					}
					path = dir + "/" + path
					path = strings.Replace(path, "//", "/", -1)
				}
				err = ImportFolderOrFile(*t, tool, path, !import_clientdir)
				if err != nil {
					fmt.Println(err.Error())
					os.Exit(1)
				}

			})

		},
	}

	snap.PersistentFlags().BoolVar(&import_clientdir, "importClient", false, "Import to Client dir")
	return snap
}

func ImportFolderOrFile(t internal.Tezos, tool *internal.TezTool, src string, data bool) error {

	id, _ := tool.GetNetwork(t.Name)
	t.DockerNetwork = id

	datavol := ""
	var err error
	if !data {
		fmt.Println("Importing to Client src...")
		datavol, err = tool.GetClientVolume(t.Name)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println("Importing to Data Volume....")
		datavol, err = tool.GetDataVolume(t.Name)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	output, err := tool.Import(src, datavol)

	if err != nil {
		return err
	}

	fmt.Println(string(output))

	return err
}

func ImportFromVolume(t internal.Tezos, tool *internal.TezTool, volume string) error {
	id, _ := tool.GetNetwork(t.Name)
	t.DockerNetwork = id

	datavol := ""
	var err error
	if strings.Contains(volume, "_client") {
		fmt.Println("Importing to Client volume...")
		datavol, err = tool.GetClientVolume(t.Name)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		fmt.Println("Importing to Data Volume....")
		datavol, err = tool.GetDataVolume(t.Name)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	output, err := tool.CopyVolumes(volume, datavol)

	if err != nil {
		return err
	}

	fmt.Println(string(output))

	return err
}

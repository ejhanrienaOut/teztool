package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"os"
	"os/exec"
	"strings"
)

func GetConfigCommand(c *dig.Container, name string) *cobra.Command {
	var config = &cobra.Command{
		Use:   "config",
		Short: "Manage config file",
	}

	config.AddCommand(GetConfigShowCommand(c, name))
	config.AddCommand(GetConfigApplyCommand(c, name))
	config.AddCommand(GetWrapperCommand(c, name, "update", "Update configuration", "tezos-client config update"))
	return config
}

func GetConfigShowCommand(c *dig.Container, name string) *cobra.Command {
	var config = &cobra.Command{
		Use:   "show",
		Short: "Show config File",
		Run: func(cmd *cobra.Command, args []string) {
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				res, err := docker.ReadFileInContainer("teztool_"+name, "/home/tezos/.tezos-node/config.json")
				if err == nil {
					fmt.Println(string(res))
				} else {
					fmt.Println(internal.GetEmptyConfig())
				}
			})

		},
	}
	return config
}

func GetConfigApplyCommand(c *dig.Container, name string) *cobra.Command {
	var config = &cobra.Command{
		Use:   "apply [file]",
		Short: "Apply config file",
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				fmt.Println("Usage: config apply [file]")
				return
			}
			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {
				file := args[0]
				if os.Getenv("MODE") == "dind" {
					// if i am inside a container, check mounted path
					file = strings.Replace(file, "./", "", 1)
					if file[1] == '/' {
						file = "/mnt/pwd" + file
					} else {
						file = "/mnt/pwd/" + file
					}
				}

				cmd := exec.Command("docker", "cp", file, "teztool_"+name+":/home/tezos/.tezos-node/config.json")
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				cmd.Stdin = os.Stdout

				cmd.Run()

				id, err := docker.GetContainerId("teztool_" + name)
				if err == nil {
					docker.ReStartContainer(id)
				}
			})

		},
	}
	return config
}

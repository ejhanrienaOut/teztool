package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
	"strings"
)

func GetCleanCommand(c *dig.Container) *cobra.Command {
	var rm = &cobra.Command{
		Use:   "clean",
		Short: "Remove unused Containers",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				conts, _ := docker.FindContainers()
				for _, c := range conts {
					if strings.Contains(c.Names[0], "task") {
						docker.DeleteContainer(c.ID)
					}
					if strings.Contains(c.Names[0], "shell") {
						docker.DeleteContainer(c.ID)
					}
				}
			})

		},
	}

	return rm
}

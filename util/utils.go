package util

import (
	"github.com/briandowns/spinner"
	"math/rand"
	"time"
)

func GetSpinner(info string) *spinner.Spinner {
	set := []string{"[ꜩ    ]", "[ ꜩ   ]", "[  ꜩ  ]", "[   ꜩ ]", "[    ꜩ]", "[     ]"}
	s := spinner.New(set, 180*time.Millisecond)
	s.Suffix = " " + info
	_ = s.Color("blue")
	s.Start()
	return s
}

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandomString(length int) string {
	return StringWithCharset(length, charset)
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
